package com.twm.myapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;

import androidx.appcompat.app.AlertDialog;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GateWay {
    private Context ctx;

    private static String name = "";
    private static String surname = "";
    private static String user = "";
    double latitude;
    double longitude;
    static String country;
    static String region;
    static String town;

    int action = 0;

    public static ArrayList<Bitmap> imageList = new ArrayList<Bitmap>();

    public GateWay(Context ctx){
        this.ctx = ctx;
    }

    public void login1(String login, String password){
        dbWorker back = new dbWorker();
        back.execute(1, login, password);
    }

    public void login2(){
        dbWorker back = new dbWorker();
        back.execute(2);
    }

    public class dbWorker extends AsyncTask {
        ProgressDialog progress;

        @Override
        protected void onPreExecute(){
            progress = new ProgressDialog(ctx);
            progress.setCancelable(false);
            progress.setIndeterminate(false);
            progress.setMessage("Loading...");

            progress.show();
        }

        @Override
        protected String doInBackground(Object[] param){
            String finalMsg = "";
            String cible = "";

            switch ((int)param[0]){
                case 1:
                    action = 1;
                    cible = "https://androidcurtis.000webhostapp.com/weather/login.php";

                    try {
                        URL url = new URL(cible);
                        HttpURLConnection con = (HttpURLConnection)url.openConnection();

                        con.setDoInput(true);
                        con.setDoOutput(true);
                        con.setRequestMethod("POST");

                        OutputStream outs = con.getOutputStream();
                        BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));

                        String msg = URLEncoder.encode("user", "utf-8") + "="
                                + URLEncoder.encode((String)param[1], "utf-8") +
                                "&" + URLEncoder.encode("pw", "utf-8") + "="
                                + URLEncoder.encode((String)param[2], "utf-8");

                        bufw.write(msg);
                        bufw.flush();
                        bufw.close();
                        outs.close();

                        InputStream ins = con.getInputStream();
                        BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));

                        String line;
                        StringBuffer sbuff = new StringBuffer();

                        while((line = bufr.readLine()) != null){
                            sbuff.append(line + "\n");

                            finalMsg = sbuff.toString();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;

                case 2:
                    action = 2;
                    getImages();
                    break;
            }
            return finalMsg;
        }

        @Override
        protected void onPostExecute(Object o){

            switch (action){
                case 1:
                    String msgLogin = (String)o;
                    msgLogin = msgLogin.replace("\n", "");

                    AlertDialog alert;
                    alert = new AlertDialog.Builder(ctx).create();

                    if(msgLogin.contains("#")){
                        String [] data = msgLogin.split("#");
                        user = data[0];
                        name = data[1];
                        surname = data[2];

                        dbWorker db = new dbWorker();
                        db.execute(2);
                    }

                    else if(msgLogin.equals("")){
                        progress.cancel();
                        alert.setTitle("Wrong login");
                        alert.setMessage("Entered login and/or password incorrect");
                        alert.show();
                    }

                    else{
                        progress.cancel();
                        alert.setTitle("Login error");
                        alert.setMessage("There was a problem why trying to login. Try again!");
                        alert.show();
                    }
                    break;

                case 2:
                    GpsLocationTracker gps = new GpsLocationTracker(ctx);
                    final Geocoder geocoder = new Geocoder(ctx);

                    if(gps.canGetLocation) {
                        List<Address> addressList = null;

                        try {
                            addressList = geocoder.getFromLocation(gps.getLatitude(), gps.getLongitude(), 1);

                        } catch (IOException e) {
                            e.printStackTrace();
                            System.out.println(e);
                        }

                        country = addressList.get(0).getCountryName();
                        region = addressList.get(0).getAdminArea();
                        town = addressList.get(0).getLocality();
                    }

                    else{
                        town = "Location not found";
                    }

                    Intent i = new Intent(ctx, HomeActivity.class);
                    ctx.startActivity(i);
                    break;
            }
        }
    }

    private void getImages(){
        String url = "https://www.shutterstock.com/search/montreal";
        URL obj = null;
        try {
            obj = new URL(url);
            HttpURLConnection http = (HttpURLConnection)obj.openConnection();
            http.setRequestMethod("GET");
            http.addRequestProperty("User-Agent", "Mozilla");

            BufferedReader br = new BufferedReader(new InputStreamReader(http.getInputStream()));
            String input;
            StringBuffer sb = new StringBuffer();

            while((input = br.readLine()) != null) {
                sb.append(input + "\n");
            }

            String s = sb.toString();  //above html content
            Pattern p = Pattern.compile("<img [^>]*src=[\\\"']([^\\\"^']*)");
            Matcher m = p.matcher (s);

            while (m.find()) {
                String src = m.group();
                int startIndex = src.indexOf("src=") + 5;
                String srcTag = src.substring(startIndex, src.length());
                //System.out.println( srcTag );
                imageList.add(getBitMap(srcTag));
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static Bitmap getBitMap(String imgUrl) {
        try {
            URL url = new URL(imgUrl);
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            System.out.println(e.toString());
            return null;
        }
    }
}
