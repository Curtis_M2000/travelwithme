package com.twm.myapplication;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

public class MyAdapter extends ArrayAdapter<Bitmap> {
    ArrayList<Bitmap> imageList = new ArrayList<Bitmap>();
    Context ctx;

    public MyAdapter(Context context, int textViewResourceId, ArrayList<Bitmap> objects){
        super(context, textViewResourceId, objects);
        imageList = objects;
        this.ctx = context;
    }

    @Override
    public int getCount(){
        return super.getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.list_view_items, null);

        ImageView imageView = (ImageView)v.findViewById(R.id.galleryimg);
        imageView.setImageBitmap(imageList.get(position));

        final int pos = position;

        imageView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent intent = new Intent(ctx, ImageFullScreenActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("BitmapImage", imageList.get(pos));
                ctx.getApplicationContext().startActivity(intent);
            }
        });
        return v;
    }
}
