package com.twm.myapplication;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

public class MyAdapterImage extends ArrayAdapter<Bitmap> {
    ArrayList<Bitmap> imageList = new ArrayList<Bitmap>();
    Context ctx;

    public MyAdapterImage(Context context, int textViewResourceId, ArrayList<Bitmap> objects){
        super(context, textViewResourceId, objects);
        imageList = objects;
        this.ctx = context;
    }

    @Override
    public int getCount(){
        return super.getCount();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.list_view_items, null);

        ImageView imageView = (ImageView)v.findViewById(R.id.galleryimg);
        imageView.setImageBitmap(imageList.get(position));

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return v;
    }
}

