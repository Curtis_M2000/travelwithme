package com.twm.myapplication;


import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.GridView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GaleryFragment extends Fragment {
    GridView simpleItemList;
    ArrayList<Bitmap> imageList = new ArrayList<>();
    ArrayList<Bitmap> searchList = new ArrayList<>();
    MyAdapter myAdapter;
    String text = "";
    Context ctx;

    public GaleryFragment(Context ctx) {
        this.ctx = ctx;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_galery, container, false);
        imageList = GateWay.imageList;
        simpleItemList = (GridView)view.findViewById(R.id.myGrid);

        setImage();

        final EditText searchText = (EditText)view.findViewById(R.id.searchbar);
        searchText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_LEFT = 0;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (searchText.getRight() - searchText.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        searchText.setText("");
                        return true;
                    }

                    else if((event.getRawX()+searchText.getPaddingLeft()) <= (searchText.getCompoundDrawables()[DRAWABLE_LEFT].getBounds().width()+searchText.getLeft())){
                        /*InputMethodManager inputMethodManager =(InputMethodManager)HomeActivity.getSystemService(HomeActivity.INPUT_METHOD_SERVICE);
                        inputMethodManager.hideSoftInputFromWindow(searchText.getWindowToken(), 0);*/
                        return true;
                    }
                }
                return false;
            }
        });

        searchText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    text = searchText.getText().toString();
                    bgWorker back = new bgWorker();
                    back.execute();
                    return true;
                }
                return false;
            }
        });
        return view;
    }

    private void setImage(){
        myAdapter = new MyAdapter(getActivity().getApplicationContext(),R.layout.list_view_items,imageList);
        simpleItemList.setAdapter(myAdapter);
    }

    public class bgWorker extends AsyncTask {
        ProgressDialog progress;
        @Override
        public void onPreExecute(){
            simpleItemList.setAdapter(null);

            progress = new ProgressDialog(ctx);
            progress.setCancelable(false);
            progress.setIndeterminate(false);
            progress.setMessage("Searching for " + text);
            progress.show();
        }

        @Override
        public String doInBackground(Object[] param){
            searchList = getImagesWithUrl(text);
            return null;
        }

        @Override
        public void onPostExecute(Object o){
            myAdapter = new MyAdapter(getActivity().getApplicationContext(),R.layout.list_view_items,searchList);
            simpleItemList.setAdapter(myAdapter);
            progress.cancel();
        }
    }

    public static ArrayList<Bitmap> getImagesWithUrl(String text){
        text.replace(" ", "+");
        String url = "https://www.shutterstock.com/search/" + text;
        System.out.println(url);
        ArrayList<Bitmap> searchList = new ArrayList<Bitmap>();
        URL obj = null;
        try {
            obj = new URL(url);
            HttpURLConnection http = (HttpURLConnection)obj.openConnection();
            http.setRequestMethod("GET");
            http.addRequestProperty("User-Agent", "Mozilla");

            BufferedReader br = new BufferedReader(new InputStreamReader(http.getInputStream()));
            String input;
            StringBuffer sb = new StringBuffer();

            while((input = br.readLine()) != null) {
                sb.append(input + "\n");
            }

            String s = sb.toString();  //above html content
            Pattern p = Pattern.compile("<img [^>]*src=[\\\"']([^\\\"^']*)");
            Matcher m = p.matcher (s);

            while (m.find()) {
                String src = m.group();
                int startIndex = src.indexOf("src=") + 5;
                String srcTag = src.substring(startIndex, src.length());
                System.out.println( srcTag );
                searchList.add(GateWay.getBitMap(srcTag));
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return searchList;
    }
}
